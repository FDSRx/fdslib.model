﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model
{
    /// <summary>
    /// Interface that adds a System.String Id (object identifier) and a System.Guid as a universal object identifier.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// The unique identifier of the entity.
        /// </summary>
        string Id { get; set; }

        /// <summary>
        /// A globally unique identifier of the entity.
        /// </summary>
        Guid Guid { get; set; }
    }
}
