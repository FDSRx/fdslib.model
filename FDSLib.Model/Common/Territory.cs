﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model.Common
{
    /// <summary>
    /// Represents an area of land under the jurisdiction of a government.
    /// </summary>    
    public class Territory
    {
        public Territory()
        {
            Name = String.Empty;
            Abbreviation = String.Empty;
            Code = String.Empty;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="name">Name of the territory.</param>
        /// <param name="abbreviation">Abbreviation for the territory name.</param>
        /// <param name="code">The code representing the territory.</param>
        public Territory(string name, string abbreviation, string code)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            Name = name;
            Abbreviation = abbreviation;
            Code = code;
        }

        /// <summary>
        /// The name of the territory.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The abbreviated name of the territory.
        /// </summary>
        public string Abbreviation { get; private set; }

        /// <summary>
        /// The code representing the territory such as a FIPS code or a country code (en-us).
        /// </summary>
        public string Code { get; private set; }
    }
}
