﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model.Common
{
    public interface ICulture
    {
        /// <summary>
        /// Neutral or full code that recognizes language and/or region (e.g. en or en-US).
        /// </summary>
        string CultureInfoCode { get; set; }

        /// <summary>
        /// Name of culture (e.g. English, Spanish, etc.).
        /// </summary>
        string Language { get; set; }

        /// <summary>
        /// Gets the neutral code that does not include region (e.g. en, es, fr, etc.)
        /// </summary>
        string GetNeutralLanguageCode();
    }
}
