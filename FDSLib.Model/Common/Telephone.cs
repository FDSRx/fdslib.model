﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using FDSLib.Common.Helpers;

namespace FDSLib.Model.Common
{
    /// <summary>
    /// An object representing a telephone endpoint used to contact a person through a phone number.
    /// </summary>
    public class Telephone
    {
        /// <summary>
        /// A Phone number of varying length available to support international numbers not 
        /// regulated by the FCC or any other commonly used international format.
        /// </summary>
        public string VariableLengthPhoneNumber { get; set; }

        /// <summary>
        /// The three digit code regulated by the FCC used to designaate a specific geographic area such as a city
        /// or territory.  For (333) 555-3232, 333 is the area code.
        /// </summary>
        public int AreaCode { get; set; }

        /// <summary>
        /// The three digit number representing the local telephone exchange or rate center.  For (333) 555-3232, 555 is the prefix.
        /// </summary>
        public int Prefix { get; set; }

        /// <summary>
        /// The number assigned to the phone line at the switch level.  For (333) 555-3232, 3232 is the line number.
        /// </summary>
        public int LineNumber { get; set; }

        /// <summary>
        /// The prefix or national code assigned to specific countries.  For +1 (333) 555-3232 or 13335553232, +1 or 1
        /// is the country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// A sequence of digits used to call another telephone over a telephone line or telephone network.
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Instantiates a new Telephone object.
        /// </summary>
        public Telephone()
        {
            this.VariableLengthPhoneNumber = string.Empty;
            this.Number = String.Empty;
            this.AreaCode = 0;
            this.Prefix = 0;
            this.LineNumber = 0;
        }

        /// <summary>
        /// Construct a telephone object using a 10 digit numeric phone number.
        /// </summary>
        /// <param name="phoneNumber">A 10 digit phone number.</param>
        public Telephone(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                throw new ArgumentNullException("phoneNumber");
            }

            //Remove phone number formatting.
            phoneNumber = StringHelper.RemoveNonNumericChars(phoneNumber);

            //Is the number 10 digits and all numeric?  If not throw a format exception.
            if (phoneNumber.Length != 10 && !StringHelper.IsNumber(phoneNumber))
            {
                throw new FormatException(phoneNumber);
            }

            this.VariableLengthPhoneNumber = phoneNumber;
            this.Number = phoneNumber;
            this.AreaCode = Convert.ToInt32(phoneNumber.Substring(0, 3));
            this.Prefix = Convert.ToInt32(phoneNumber.Substring(3, 3));
            this.LineNumber = Convert.ToInt32(phoneNumber.Substring(6, 4));
        }

        /// <summary>
        /// Construct a telephone object using a 10 digit numeric phone number and a country code.
        /// </summary>
        /// <param name="phoneNumber">A 10 digit phone number.</param>
        /// <param name="countryCode">The country code.</param>
        public Telephone(string phoneNumber, string countryCode)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                throw new ArgumentNullException("phoneNumber");
            }

            if (string.IsNullOrEmpty(countryCode))
            {
                throw new ArgumentNullException("countryCode");
            }

            //Remove phone number formatting.
            phoneNumber = StringHelper.RemoveNonNumericChars(phoneNumber);

            //Is the number 10 digits and all numeric?  If not throw a format exception.
            if (phoneNumber.Length != 10 && !StringHelper.IsNumber(phoneNumber))
            {
                throw new FormatException(phoneNumber);
            }

            this.VariableLengthPhoneNumber = phoneNumber;
            this.Number = phoneNumber;
            this.AreaCode = Convert.ToInt32(phoneNumber.Substring(0, 3));
            this.Prefix = Convert.ToInt32(phoneNumber.Substring(3, 3));
            this.LineNumber = Convert.ToInt32(phoneNumber.Substring(6, 4));
            this.CountryCode = countryCode;
        }

        /// <summary>
        /// Indicates whether the telephone number is a valid phone number.
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return !String.IsNullOrEmpty(this.Number) && IsValidPhoneNumber(this.Number);
        }

        /// <summary>
        /// Indicates whether the telephone number is a valid phone number.
        /// </summary>
        /// <param name="number">Telephone number.</param>
        /// <returns></returns>
        public static bool IsValidPhoneNumber(string number)
        {
            // Return true if emailAddress is in valid e-mail format.
            return !String.IsNullOrEmpty(number) && Regex.IsMatch(number, @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$");
        }

        /// <summary>
        /// Formats the phone number as 1(333)555-3232.  If the country code is omitted, then (333)555-3232.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}",
                string.IsNullOrEmpty(CountryCode) ? "" : CountryCode,
                "(",
                AreaCode,
                ") ",
                Prefix,
                "-",
                LineNumber
                );
        }
    }
}
