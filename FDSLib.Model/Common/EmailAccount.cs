﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FDSLib.Model.Common
{
    /// <summary>
    /// Identifies a specific email mailbox to which email messages may be delivered.
    /// </summary>
    public class EmailAccount
    {
        /// <summary>
        /// Instantiates a new EmailAccount object.
        /// </summary>
        public EmailAccount()
        {
            this.Address = String.Empty;
            this.Username = String.Empty;
            this.Domain = String.Empty;
        }

        /// <summary>
        /// Create an EmailAccount object with a valid email address.
        /// </summary>
        /// <param name="emailAddress">A unique 256 character length web address that correlates to an online mailbox.</param>
        public EmailAccount(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            if (!IsValidEmail(emailAddress))
            {
                throw new FormatException(emailAddress);
            }

            this.Address = emailAddress;

            string[] parts = emailAddress.Split('@');

            this.Username = parts[0];
            this.Domain = parts[1];
            this.TopLevelDomain = parts[2];
        }

        /// <summary>
        /// Validates the email address for this email account.
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return !string.IsNullOrEmpty(Address) && IsValidEmail(Address);
        }

        /// <summary>
        /// Verify that the Address is a valid email format. 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        /// <remarks>
        /// From http://msdn.microsoft.com/en-us/library/01escwtf(v=vs.95).aspx 
        /// </remarks>
        public static bool IsValidEmail(string emailAddress)
        {
            // Return true if emailAddress is in valid e-mail format.
            return Regex.IsMatch(emailAddress,
                    @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

        /// <summary>
        /// The email address for the account.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The part of the email before the @ sign.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The domain name to the right of the @ sign which follows strict requirements for hostname.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// The three characters that follow after the "." after the "@" sign. The top-level domain can be .com, .net, .org, .edu, .gov, etc.
        /// </summary>
        public string TopLevelDomain { get; set; }
    }
}
