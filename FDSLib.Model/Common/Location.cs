﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model.Common
{
    /// <summary>
    /// Represents a geographic location that is determined by latitude and longitude coordinates.
    /// </summary>
    public class Location
    {
        public Location()
        {
            Latitude = 0;
            Longitude = 0;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="latitude">The latitude expressed as a degree or some corresponding difference in time (such as minutes).</param>
        /// <param name="longitude">The longitude expressed as a degree or some corresponding difference in time (such as minutes).</param>
        public Location(decimal latitude, decimal longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        /// <summary>
        /// The angular distance, in degree or minutes, of a place north or south of the earth's equator.
        /// </summary>
        public decimal Latitude { get; set; }

        /// <summary>
        /// The angular distance, in degree or minutes, of a place east or west of some prime meridian
        /// such as that of Greenwich, England.
        /// </summary>
        public decimal Longitude { get; set; }
    }


}
