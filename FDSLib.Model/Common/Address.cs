﻿using FDSLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model.Common
{
    public class Address : Entity<int>, IAddress
    {

        #region IAddress members

        /// <summary>
        /// The information identifying the exact address within the given Postal Code such as a street name
        /// and number or P.O. Box.
        /// </summary>
        public string Line1 { get; set; }

        /// <summary>
        /// Supplemental address information such as Suite number, Apartment number, etc.  Used to further qualify a mailing address.
        /// </summary>
        public string Line2 { get; set; }

        /// <summary>
        /// A specific incorporated municipality.  Commonly known as a city, town, or village.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// A territorial subdivision of a country having a central civil government or authority.  
        /// Commonly known as states, regions, or provinces.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// A code utilized to further qualify an address for the purpose of sorting and filtering mail.
        /// </summary>
        public string PostalCode { get; set; }

        #endregion

        /// <summary>
        /// Represents an area of land under the jurisdiction of a government.  A territory occupied by a governed people.  A nation.
        /// </summary>
        public Territory Country { get; set; }

        /// <summary>
        /// An administrative or political subdivision or area such as a county, parish, or township.
        /// </summary>
        public Territory Municipality { get; set; }

        /// <summary>
        /// The geographic location of an address.
        /// </summary>
        public Location Location { get; set; }

        /// <summary>
        /// Initializes a new instance of the Address object.
        /// </summary>
        public Address()
        {
            this.Line1 = String.Empty;
            this.Line2 = String.Empty;
            this.City = String.Empty;
            this.State = String.Empty;
            this.PostalCode = String.Empty;
            this.Country = new Territory();
            this.Municipality = new Territory();
            this.Location = new Location();
        }

        /// <summary>
        /// Initializes a new instance of the Address object.
        /// </summary>
        /// <param name="line1">The information identifying the exact address within the given Postal Code such as a street name
        /// and number or P.O. Box.</param>
        /// <param name="line2">Supplemental address information such as Suite number, Apartment number, etc.  Used to further qualify a mailing address.</param>
        /// <param name="city">A specific incorporated municipality.  Commonly known as a city, town, or village.</param>
        /// <param name="state">A territorial subdivision of a country having a central civil government or authority.  
        /// Commonly known as states, regions, or provinces.</param>
        /// <param name="postalCode">A code utilized to further qualify an address for the purpose of sorting and filtering mail.</param>
        public Address(string line1, string line2, string city, string state, string postalCode) :
            this()
        {
            if (String.IsNullOrEmpty(line1))
            {
                throw new ArgumentNullException("line1");
            }

            if (String.IsNullOrEmpty(state))
            {
                throw new ArgumentNullException("state");
            }

            if (String.IsNullOrEmpty(postalCode))
            {
                throw new ArgumentNullException("postalCode");
            }


            this.Line1 = line1;
            this.Line2 = line2;
            this.City = city;
            this.State = state;
            this.PostalCode = PostalCode;
        }


        /// <summary>
        /// Initializes a new instance of the Address object.
        /// </summary>
        /// <param name="line1">The information identifying the exact address within the given Postal Code such as a street name
        /// and number or P.O. Box.</param>
        /// <param name="line2">Supplemental address information such as Suite number, Apartment number, etc.  Used to further qualify a mailing address.</param>
        /// <param name="city">A specific incorporated municipality.  Commonly known as a city, town, or village.</param>
        /// <param name="state">A territorial subdivision of a country having a central civil government or authority.  
        /// Commonly known as states, regions, or provinces.</param>
        /// <param name="postalCode">A code utilized to further qualify an address for the purpose of sorting and filtering mail.</param>
        /// <param name="country">Represents an area of land under the jurisdiction of a government.  A territory occupied by a governed people.  A nation.</param>
        /// <param name="municipality">An administrative or political subdivision or area such as a county, parish, or township.</param>
        /// <param name="location">The geographic location of an address.</param>
        public Address(string line1, string line2, string city, string state, string postalCode, Territory country, Territory municipality, Location location)
        {
            if (String.IsNullOrEmpty(line1))
            {
                throw new ArgumentNullException("line1");
            }

            if (String.IsNullOrEmpty(state))
            {
                throw new ArgumentNullException("state");
            }

            if (String.IsNullOrEmpty(postalCode))
            {
                throw new ArgumentNullException("postalCode");
            }

            this.Line1 = line1;
            this.Line2 = line2;
            this.City = city;
            this.State = state;
            this.PostalCode = PostalCode;
            this.Country = country;
            this.Municipality = municipality;
            this.Location = location;
        }

        /// <summary>
        /// Determines whether this string and System.String object have the same value.
        /// </summary>
        /// <param name="obj">The object to compare to this instance.</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return obj != null
                && obj is Address
                && this == (Address)obj;
        }

        /// <summary>
        /// Returns the hashcode for this object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.Line1.GetHashCode()
                ^ this.City.GetHashCode()
                ^ this.State.GetHashCode()
                ^ this.PostalCode.GetHashCode();
        }

        /// <summary>
        /// Determines whether this string and System.String object have the same value.
        /// </summary>
        /// <param name="addr1">Address object.</param>
        /// <param name="addr2">Address object.</param>
        public static bool operator ==(Address addr1, Address addr2)
        {
            //Check for both null (cast to object to avoid recursive loop)
            if ((object)addr1 == null && (object)addr2 == null)
            {
                return true;
            }

            //Check for either of them equal to null
            if ((object)addr1 == null || (object)addr2 == null)
            {
                return false;
            }

            //Make comparison on data
            if (addr1.Line1 != addr2.Line1
                || addr1.City != addr2.City
                || addr1.State != addr2.State
                || addr1.PostalCode != addr2.PostalCode)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determines whether this string and System.String object do not have the same value.
        /// </summary>
        /// <param name="addr1">Address object.</param>
        /// <param name="addr2">Address object.</param>
        /// <returns></returns>
        public static bool operator !=(Address addr1, Address addr2)
        {
            return !(addr1 == addr2);
        }

        /// <summary>
        /// Returns the address in properly formatted string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}",
                this.Line1,
                "\r\n",
                this.City,
                ", ",
                this.State,
                " ",
                this.PostalCode);
        }
    }
}
