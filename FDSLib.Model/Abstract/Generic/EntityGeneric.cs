﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model
{
    /// <summary>
    /// Generic abstract base class that adds a strongly typed Id (object identifier) and a System.Guid as a universal object identifier to an object.
    /// </summary>
    /// <typeparam name="T">The type of Id</typeparam>
    public abstract class Entity<T> : IEntity<T>, IComparable<Entity<T>>
    {
        //public const string ID = "Id";

        #region IEntity Members

        /// <summary>
        /// The unique identifier of the entity.
        /// </summary>
        public T Id { get; set; }

        /// <summary>
        ///  A globally unique identifier of the entity.
        /// </summary>
        public Guid Guid { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the Entity(of T) object.
        /// </summary>
        protected Entity()
        {
            this.Guid = Guid.NewGuid();
        }

        /// <summary>
        /// Compares this instance with a sepcified System.String object and indicates whether this instance precedes, follows, or
        /// appears in the same position in the sort order as the specified System.String.
        /// </summary>
        /// <param name="other">An object that evaluates to a System.String.</param>
        /// <returns></returns>
        public int CompareTo(Entity<T> other)
        {
            if (IsPersistent())
            {
                return this.Id.ToString().CompareTo(other.Id.ToString());
            }
            return 0;
        }

        /// <summary>
        /// Compares the globally unique identifier of this instance with a sepcified Entity(of T) object's globally unique identifier
        /// and indicates whether this instance precedes, follows, or
        /// appears in the same position in the sort order as the specified Entity(of T).
        /// </summary>
        /// <param name="other">An object that evaluates to a System.String.</param>
        /// <returns></returns>
        public int CompareGuidTo(Entity<T> other)
        {
            if (IsGuidPersistent())
            {
                return this.Guid.ToString().CompareTo(other.Guid.ToString());
            }
            return 0;
        }

        /// <summary>
        /// Determines whether this string and System.String object have the same value.
        /// </summary>
        /// <param name="obj">The string to compare to this instance.</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (IsPersistent())
            {
                var entity = obj as Entity<T>;
                return (entity != null) && (this.Id.ToString() == entity.Id.ToString());
            }

            return base.Equals(obj);
        }

        /// <summary>
        /// Determines whether this string representation of the globally unique identifier and the globally unique identifier of the
        /// System.String object have the same value.
        /// </summary>
        /// <param name="obj">The string to compare to this instance.</param>
        /// <returns></returns>
        public bool GuidEquals(object obj)
        {
            if (IsGuidPersistent())
            {
                var entity = obj as Entity<T>;
                return (entity != null) && (this.Guid.ToString() == entity.Guid.ToString());
            }

            return base.Equals(obj);
        }

        /// <summary>
        /// Returns the hashcode for this object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return IsPersistent() ? this.Id.GetHashCode() : base.GetHashCode();
        }

        /// <summary>
        /// Returns the hashcode of the guid for this object.
        /// </summary>
        /// <returns></returns>
        public int GetGuidHashCode()
        {
            return IsGuidPersistent() ? this.Guid.GetHashCode() : base.GetHashCode();
        }

        /// <summary>
        /// Determines whether this string and System.String object have the same value.
        /// </summary>
        /// <param name="entity1">Entity(of T) object.</param>
        /// <param name="entity2">Entity(of T) object.</param>
        /// <returns></returns>
        public static bool operator ==(Entity<T> entity1, Entity<T> entity2)
        {
            if ((object)entity1 == null && (object)entity2 == null)
            {
                return true;
            }

            if ((object)entity1 == null || (object)entity2 == null)
            {
                return false;
            }

            if (entity1.Id.ToString() == entity2.Id.ToString())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether this string and System.String object do not have the same value.
        /// </summary>
        /// <param name="entity1">Entity(of T) object.</param>
        /// <param name="entity2">Entity(of T) object.</param>
        /// <returns></returns>
        public static bool operator !=(Entity<T> entity1, Entity<T> entity2)
        {
            return (!(entity1 == entity2));
        }

        /// <summary>
        /// Returns the System.String representation of the entities Id property.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (this.IsPersistent())
            {
                return this.Id.ToString();
            }

            return String.Empty;
        }

        /// <summary>
        /// Indicates if the Entity(of T) object contains a valid persistent value (i.e. the value is not of its default type).
        /// </summary>
        /// <returns></returns>
        private bool IsPersistent()
        {
            //check for null reference to the object.
            if ((object) this.Id == null)
            {
                return false;
            }

            //ensure the string representation of the object is not null or empty.
            if (String.IsNullOrEmpty(this.Id.ToString()))
            {
                return false;
            }

            var t = default(T);

            var result = (object) t == null ? null : t.ToString();

            return (this.Id.ToString() != result);
        }

        /// <summary>
        /// Indicates if the Entity(of T) object contatins a valid, persistent, globally unique identifier (i.e. the value of the guid is not null).
        /// </summary>
        /// <returns></returns>
        private bool IsGuidPersistent()
        {
            return !(this.Guid != Guid.Empty);
        }


    }

}
