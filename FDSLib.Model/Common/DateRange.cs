﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FDSLib.Common.Extensions;


namespace FDSLib.Model.Common
{
    ///<summary>
    /// Represents a range of two dates.
    /// </summary>
    public class DateRange
    {
        /// <summary>
        /// Gets/Sets the date respresenting the start of this range.
        /// </summary>
        public DateTime? Start { get; set; }

        /// <summary>
        /// Gets/Sets the date respresenting the end of this range.
        /// </summary>
        public DateTime? End { get; set; }

        /// <summary>
        /// Instantiates a new DateRange object using the supplied start and end dates.
        /// </summary>
        public DateRange() { }

        /// <summary>
        /// Instantiates a new DateRange object using the supplied start and end dates.
        /// </summary>
        /// <param name="startDate">The start of the range.</param>
        /// <param name="endDate">The end of the range.</param>
        public DateRange(DateTime? startDate, DateTime? endDate)
        {
            this.Start = startDate;
            this.End = endDate;
        }

        /// <summary>
        /// Instantiates a new DateRange object using the supplied start and end dates.
        /// </summary>
        /// <param name="startDate">A string representation of the start of the range (e.g. 1/1/2014).</param>
        /// <param name="endDate">A string representation of the end of the range (e.g. 1/31/2014).</param>
        public DateRange(string startDate, string endDate)
        {
            this.Start = startDate.ToTypeOrNull<DateTime>();
            this.End = endDate.ToTypeOrNull<DateTime>();
        }

        /// <summary>
        /// Returns a string representation of this range in the following format: dd/MM/yyyy - dd/MM/yyyy
        /// </summary>
        /// <returns>A string representation of this range.</returns>
        public override string ToString()
        {
            DateTime? dateTime = this.Start;

            string str1 = dateTime.HasValue ? dateTime.Value.ToString("dd/MM/yyyy") : DateTime.MinValue.ToShortDateString();
            string str2 = " - ";
            dateTime = this.End;
            string str3 = dateTime.HasValue ? dateTime.Value.ToString("dd/MM/yyyy") : DateTime.MaxValue.ToShortDateString();

            return String.Format("{0}{1}{2}", str1, str2, str3);
        }

        /// <summary>
        /// Returns the TimeSpan that this DateRange covers.
        /// </summary>
        /// <returns>A TimeSpan.</returns>
        public TimeSpan ToTimeSpan()
        {
            if (this.Start.HasValue && this.End.HasValue)
            {
                return new TimeSpan(this.End.Value.Ticks - this.Start.Value.Ticks);
            }

            return new TimeSpan();
        }

        /// <summary>
        /// Confirms if the supplied date falls between the dates of this range.
        /// </summary>
        /// <param name="date">The date to check for.
        /// <returns>true if the date is between the start and end dates of this range (inclusive).</returns>
        public bool IsInRange(DateTime date)
        {
            if (this.Start.HasValue && this.End.HasValue)
            {
                return date >= this.Start.Value && date <= this.End.Value;
            }

            return false;
        }

        /// <summary>
        /// Confirms if the supplied dates fall between the dates of this range.
        /// </summary>
        /// <param name="dates">An array of dates to check for.
        /// <returns>true if -ALL- dates supplied fall between the start date and end date of this range.</returns>
        public bool IsInRange(params DateTime[] dates)
        {
            return dates.All(d => IsInRange(d));
        }

        /// <summary>
        /// Checks if the supplied DateRange object is between the dates of this range.
        /// </summary>
        /// <param name="range">The DateRange to check.
        /// <returns>true if the DateRange starts and ends within this DateRange.</returns>
        public bool IsInRange(DateRange range)
        {
            if (range == null)
            {
                throw new ArgumentNullException("dates");
            }

            return IsInRange(range.Start.Value, range.End.Value);
        }

        /// <summary>
        /// Indicates whether the supplied dates create a valid range (i.e. the end date is not allowed to be less than the start date).
        /// </summary>
        /// <param name="dateStart">The start date of the range.</param>
        /// <param name="dateEnd">The end date of the range.</param>
        /// <returns></returns>
        public static bool IsValidRange(DateTime dateStart, DateTime dateEnd)
        {
            if (dateStart == null)
            {
                throw new ArgumentNullException("dateStart");
            }

            if (dateEnd == null)
            {
                throw new ArgumentNullException("dateEnd");
            }

            if (dateEnd < dateStart)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates if the range is valid (i.e. the end date is not allowed to be less than the start date).
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return IsValidRange(this.Start.Value, this.End.Value);
        }

        /// <summary>
        /// Calculates a date range from the current date/time.
        /// </summary>
        /// <param name="fromNow">Number of days, or hours, or seconds, etc. from the current date/time that is used to calculate the range.</param>
        /// <returns></returns>
        public static DateRange FromNow(TimeSpan fromNow)
        {
            if (fromNow == null)
            {
                throw new ArgumentNullException("fromNow");
            }

            DateTime now = DateTime.Now;

            if (fromNow.TotalMilliseconds < 0)
            {
                return new DateRange(now.Add(fromNow), now);
            }
            else if (fromNow.TotalMilliseconds > 0)
            {
                return new DateRange(now, now.Add(fromNow));
            }

            return new DateRange(now, now);            
        }

    }
}
