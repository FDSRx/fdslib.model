﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model.Common
{
    public interface IAddress
    {
        /// <summary>
        /// The information identifying the exact address within the given Postal Code such as a street name
        /// and number or P.O. Box.
        /// </summary>
        string Line1 { get; set; }

        /// <summary>
        /// Supplemental address information such as Suite number, Apartment number, etc.  Used to further qualify a mailing address.
        /// </summary>
        string Line2 { get; set; }

        /// <summary>
        /// A specific incorporated municipality.  Commonly known as a city, town, or village.
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// A territorial subdivision of a country having a central civil government or authority.  
        /// Commonly known as states, regions, or provinces.
        /// </summary>
        string State { get; set; }

        /// <summary>
        /// A code utilized to further qualify an address for the purpose of sorting and filtering mail.
        /// </summary>
        string PostalCode { get; set; }
    }
}
