﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model
{
    /// <summary>
    /// Base abstract class that adds a System.String identifier and System.Guid as a universal identifier to an object.
    /// </summary>
    public abstract class Entity : IEntity, IComparable<Entity>
    {
        //public const string ID = "Id";

        #region IEntity Members

        /// <summary>
        /// The unique identifier of the entity.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///  A globally unique identifier of the entity.
        /// </summary>
        public Guid Guid { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the Entity object.
        /// </summary>
        protected Entity()
        {
            this.Guid = Guid.NewGuid();
        }

        /// <summary>
        /// Compares this instance with a sepcified System.String object and indicates whether this instance precedes, follows, or
        /// appears in the same position in the sort order as the specified System.String.
        /// </summary>
        /// <param name="other">An object that evaluates to a System.String.</param>
        /// <returns></returns>
        public int CompareTo(Entity other)
        {
            if (IsPersistent())
            {
                return this.Id.CompareTo(other.Id);
            }
            return 0;
        }

        /// <summary>
        /// Compares the globally unique identifier of this instance with a sepcified Entity object's globally unique identifier
        /// and indicates whether this instance precedes, follows, or
        /// appears in the same position in the sort order as the specified Entity.
        /// </summary>
        /// <param name="other">An object that evaluates to a System.String.</param>
        /// <returns></returns>
        public int CompareGuidTo(Entity other)
        {
            if (IsGuidPersistent())
            {
                return this.Guid.ToString().CompareTo(other.Guid.ToString());
            }
            return 0;
        }

        /// <summary>
        /// Determines whether this string and System.String object have the same value.
        /// </summary>
        /// <param name="obj">The string to compare to this instance.</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (IsPersistent())
            {
                var entity = obj as Entity;
                return (entity != null) && (this.Id == entity.Id);
            }

            return base.Equals(obj);
        }

        /// <summary>
        /// Determines whether this string representation of the globally unique identifier and the globally unique identifier of the
        /// System.String object have the same value.
        /// </summary>
        /// <param name="obj">The string to compare to this instance.</param>
        /// <returns></returns>
        public bool GuidEquals(object obj)
        {
            if (IsGuidPersistent())
            {
                var entity = obj as Entity;
                return (entity != null) && (this.Guid.ToString() == entity.Guid.ToString());
            }

            return base.Equals(obj);
        }

        /// <summary>
        /// Returns the hashcode for this object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return IsPersistent() ? this.Id.GetHashCode() : base.GetHashCode();
        }

        /// <summary>
        /// Returns the hashcode of the guid for this object.
        /// </summary>
        /// <returns></returns>
        public int GetGuidHashCode()
        {
            return IsGuidPersistent() ? this.Guid.GetHashCode() : base.GetHashCode();
        }

        /// <summary>
        /// Determines whether this string and System.String object have the same value.
        /// </summary>
        /// <param name="entity1">Entity object.</param>
        /// <param name="entity2">Entity object.</param>
        /// <returns></returns>
        public static bool operator ==(Entity entity1, Entity entity2)
        {
            if ((object)entity1 == null && (object)entity2 == null)
            {
                return true;
            }

            if ((object)entity1 == null || (object)entity2 == null)
            {
                return false;
            }

            if (entity1.Id == entity2.Id)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether this string and System.String object do not have the same value.
        /// </summary>
        /// <param name="entity1">Entity object.</param>
        /// <param name="entity2">Entity object.</param>
        /// <returns></returns>
        public static bool operator !=(Entity entity1, Entity entity2)
        {
            return (!(entity1 == entity2));
        }

        /// <summary>
        /// Returns the System.String representation of the entities Id property.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (this.IsPersistent())
            {
                return this.Id;
            }

            return String.Empty;
        }

        /// <summary>
        /// Indicates if the Entity object contains a valid persistent value (i.e. the value is not of its default type).
        /// </summary>
        /// <returns></returns>
        private bool IsPersistent()
        {
            if (String.IsNullOrEmpty(this.Id))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indicates if the Entity object contatins a valid, persistent, globally unique identifier (i.e. the value of the guid is not null).
        /// </summary>
        /// <returns></returns>
        private bool IsGuidPersistent()
        {
            return !(this.Guid != Guid.Empty);
        }


    }
}
