﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDSLib.Model.Common
{
    public class WebAddress
    {
        /// <summary>
        /// 'Protocol' is a set of rules a browser and a web server use to communicate with and understand each other.
        /// </summary>
        public string Protocol { get; set; }

        /// <summary>
        /// Name of a server listening to messages using the http protocol.  The name can be other than the standard "www."
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// The desired endpoint (e.g. http://www.google.com).
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Name of the domain where the web server belongs
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// The three characters that follow after the final ".". The top-level domain can be .com, .net, .org, .edu, .gov, etc.
        /// </summary>
        public string TopLevelDomain { get; set; }

        /// <summary>
        /// Instantiates a new WebAddress.
        /// </summary>
        public WebAddress() { }

    }
}
